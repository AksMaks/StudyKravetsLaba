const bcript = require("bcryptjs")
const jwt = require("jsonwebtoken")
const {check, validationResult} = require("express-validator")
//const pool = require("../models/db.js");

let db = require("../models/index.js");
const Sequelize = require('sequelize');

const ErroeHandler = require("../utils/ErrorHandler")

module.exports.Get = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}

    await db.sequelize.transaction(async  transaction => {
      let result = await db.Cat.finAll(
      {transaction: transaction}
      );
      console.log(result)
      Response.Data = result
    })

    Response.message = "Ok"
    return res.status(200).json(Response)
  }
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Search = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}
    await db.sequelize.transaction(async  transaction => {
      let result = await db.Cat.findOne({
        where: {
          Id: req.body.Data.Id
        }
      },
      {transaction: transaction}
      );
      console.log(result)
      Response.Data = result
    })
    
    Response.message = "Ok"
    return res.status(200).json(Response)
  }
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Insert = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    const {Name} = req.body.Data
    
    let Response = {}
    
    let newObject = {
      Name: Name
    }
    await db.sequelize.transaction(async  transaction => {
      let result = await db.Category.create(
        newObject,
        {transaction: transaction}
      );
      console.log(result)
      Response.message = "Запись добавлена"
      Response.Data = result
    })
    
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Update = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    const {Id, Name} = req.body.Data
   
    let newObject = {
      Name: Name
    }
    
    let Response = {}
    
    await db.sequelize.transaction(async  transaction => {
      let result = await db.Cat.update(
        newObject,
        {
          where: {
            Id: Id
          }
        },
        {transaction: transaction}
      );
      console.log(result)
      Response.message = "Обьект изменен"
      Response.Data = result
    })
    
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Delete = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}
    await db.sequelize.transaction(async  transaction => {
      let result = await db.Cat.destroy(
        {
          where: {
            Id: req.body.Data.Id
          }
        },
        {transaction: transaction}
      );
      console.log(result)
      Response.message = "Обьект удален"
      Response.Data = result
    })
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}